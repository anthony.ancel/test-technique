# Test technique React - ekWateur
## Sujet:
Le but du test était de réaliser une page web contenant un tableau regroupant les données d'une api.

## Techno:

- React
- NodeJS
- Typescript
- Axios
- React-testing-library
- Jest

## Features:

- Récupération des données (meters/pointOfDelivery) à afficher
- Choix du type d'énergie pour filtrer les valeurs
- Tri selon l'année, les informations sont affichées de la date la plus récente a la plus éloignée
- Pagination (actuellement chaque page du tableau contient un maximum de 10 entrées)

## Point d'amélioration:

- Avoir un fichier config d'environnement, pour gérer les clés d'API plus proprement
- Aller plus loin dans les tests, tester par exemple le cas où l'API retourne un objet dans le mauvais format, ou aucun retour.
- Faire la partie mobile
- Ajouter le tri selon les entrées du tableau (date, id)
- Ajouter une progression calculée selon la valeur de la date précédente
- Ajouter une courbe représentant l'évolution de la consommation au fil d'une année

## Commandes:
Installer les modules:
```sh
yarn install
```

Lancer le projet:
```sh
yarn start
```

Lancer les tests
```sh
yarn test
```
