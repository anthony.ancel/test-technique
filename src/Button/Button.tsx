import classnames from 'classnames';

import './Button.css';

type ButtonProps = {
  label?: string,
  isSelected?: boolean,
  onClick?: () => any,
  className?: string,
}

const Button = ({ label, isSelected, onClick, className}: ButtonProps) => {
  return label && onClick ? (
    <button
      className={classnames('Button', className, {'Button_selected': isSelected})}
      onClick={() => {if (!isSelected && onClick) { onClick(); }}}
    >
      {label}
    </button>
  ) : null;
}

export default Button;
