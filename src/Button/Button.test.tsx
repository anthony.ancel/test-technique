import { render, fireEvent } from "@testing-library/react";
import Button from "./Button";

const mockOnClick = jest.fn();

const mockProps = {
  label: "mock-button",
  isSelected: false,
  onClick: mockOnClick,
  className: "mock-classname",
};

test("Button component : Should displays correctly without props", () => {
  const { container } = render(<Button />);
  expect(container).toMatchInlineSnapshot(`<div />`);
});

test("Button component : Should displays correctly with props", () => {
  const { container } = render(<Button {...mockProps} />);
  expect(container).toMatchInlineSnapshot(`
      <div>
        <button
          class="Button mock-classname"
        >
          mock-button
        </button>
      </div>
    `);
});

test("Button component : Should call function onClick correctly", () => {
  const { getByText } = render(<Button {...mockProps} />);
  fireEvent.click(getByText('mock-button'));
  expect(mockOnClick).toHaveBeenCalled();
});
