import axios from 'axios';
import { useState, useEffect } from 'react';
import Button from '../Button/Button';
import { compareDesc } from 'date-fns';
import ConsoTable from '../ConsoTable/ConsoTable';

import './Dashboard.css';

// TODO: Make a environement variable
const API_URL = 'https://61b0dd073c954f001722a6c4.mockapi.io/test-react/';
const PAGINATION_LENGTH = 10;

type PodValue = {
  date: string,
  id: string,
  values?: Array<number>,
  value?: number,
  valueHP?: number,
  valueHC?: number,
}

type Pod = {
  [key: string]: Array<PodValue>;
}

type Meters = {
  energy: string;
  pointOfDelivery: string,
}

type MetersAndPod = {
  meters: Array<string>;
  pod: Pod;
}

const headers: {[key: string]: Array<string> } = {
  electricity: ['Identifiant', 'Date', 'Heures Pleines', 'Heures Creuses'],
  gas: ['Identifiant', 'Date', 'Heures Pleines'],
}

/**
 * Call API to get all meters and pod, then format them correctly for table
 */
const getAllMetersAndPod = async (): Promise<MetersAndPod> => {
  
  // get all meters
  const { data: rawMeters = [] } = await axios.get(`${API_URL}/meters`) || {};

  // loop on them for calling api with pointOfDelivery
  return await rawMeters.reduce(async (prev: Promise<MetersAndPod>, curr: Meters): Promise<MetersAndPod> => {
    const { energy, pointOfDelivery: id } = curr;

    // if no id we cant call api, with no energy we cant have a good object format
    if (!id || !energy) return prev;

    const { meters = [], pod = {} } = await prev;
    const { data: rawPod = [] } = await axios.get(`${API_URL}/${id}`) || {};

    // format all pod values to get a object with only id, date, and values in an array. ex: {value} => ['value'], {valueHP, valueHC} => ['valueHP, valueHC']
    const formattedRawPod: Array<PodValue> = (rawPod as Array<PodValue>).map((curr: PodValue) => {
      const { date, id } = curr;

      // format value according to energy type
      const values = energy === 'gas' ? [curr?.value || 0] : [curr?.valueHP || 0, curr?.valueHC || 0];

      return { id, date, values};
    }, []).sort((a, b) => compareDesc(new Date(a?.date), new Date(b?.date)));

    return { meters: [...meters, energy], pod: {...pod, [energy]: formattedRawPod}};
  }, { meters: [], pod: {}});
}

/**
 * Dashboard Component
 */
const Dashboard = () => {

  const [meters, setMeters] = useState<Array<string>>([]);
  const [pod, setPod] = useState<Pod>({});
  const [selectedMeter, setSelectedMeter] = useState<string>('');
  const [pagination, setPagination] = useState<number>(0);

  const paginationMax = pod && pod[selectedMeter] ? Math.ceil(pod[selectedMeter].length / PAGINATION_LENGTH) : 0;

  useEffect(() => {
    async function fetchData() {
      const { meters: metersTab = [], pod: podObj = {} } = await getAllMetersAndPod() || {};
      setMeters(metersTab);
      setPod(podObj);
      setSelectedMeter(metersTab[0]);
    }
    fetchData();
  }, []);

  return (
    <div className='Dashboard'>
      <p className='Dashboard__title'>Tableau de bord</p>
      <p className='Dashboard__intro'>Vos données de consommation</p>
      <div className='Dashboard__container'>
        {meters.length > 1 && <div className='Dashboard__buttons'>{/* If we have only one meter, no need to display button*/}
          { meters.map((meter, index) => (
            <Button
            label={meter}
            key={`btn-${index}`}
            isSelected={selectedMeter === meter}
            onClick={() => { setSelectedMeter(meter); setPagination(0); }}
            />
          ))}
        </div>}
        {pod && pod[selectedMeter] &&
          <ConsoTable
          className='Dashboard__table'
          header={headers[selectedMeter]}
          values={pod[selectedMeter].slice(pagination * PAGINATION_LENGTH, (pagination + 1) * PAGINATION_LENGTH)}
          />
        }
      </div>
      { paginationMax > 1 && <div className='Dashboard__pagination'>{/* If we have only one page, no need to display pagination*/}
        { [...Array(paginationMax)].map((a, index) => (
          <Button
            key={`pgn-${index}`}
            label={(index + 1).toString()}
            isSelected={index === pagination}
            onClick={() => { setPagination(index); }}
          />
        ))}
      </div>}
    </div>
  );
}

export default Dashboard;
