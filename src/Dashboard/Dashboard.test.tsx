import axios from 'axios';
import { render, waitFor, screen } from "@testing-library/react";
import Dashboard from "./Dashboard";

jest.mock('../Button/Button', () => 'mock-button');
jest.mock('../ConsoTable/ConsoTable', () => 'mock-consoTable');
jest.mock('axios');

const axiosGetMetersGas = [{'energy': 'gas', 'pointOfDelivery': 'mock-pod'}];
const axiosGetPodGas = [
  {'date': '2021-11-01T10:10:10.111Z', 'value': '95', 'id': '1'},
  {'date': '2021-08-01T10:10:10.111Z', 'value': '95', 'id': '2'}
];

const axiosGetMetersElec = [{'energy': 'electricity', 'pointOfDelivery': 'mock-pod'}];
const axiosGetPodElec = [
  {'date': '2021-11-01T10:10:10.111Z', 'valueHP': '95', 'valueHC': '65', 'id': '1'},
  {'date': '2021-09-01T10:10:10.111Z', 'valueHP': '75', 'valueHC': '35', 'id': '2'}
];

const axiosGetMetersBoth = [{'energy': 'gas', 'pointOfDelivery': 'mock-pod'}, {'energy': 'electricity', 'pointOfDelivery': 'mock-pod'}];

test("Dashboard component : Should displays correctly without props", async () => {
    const { container } = render(<Dashboard />);
    expect(await screen.findByText(/Tableau de bord/i)).toBeInTheDocument();
    expect(container).toMatchSnapshot();
});

test("Dashboard component : Should displays correctly with gas values", async () => {
  axios.get = jest.fn()
    .mockReturnValueOnce( Promise.resolve({ data: axiosGetMetersGas }) )
    .mockReturnValueOnce( Promise.resolve({ data: axiosGetPodGas }) );
 
  const { container } = render(<Dashboard />);
  expect(await screen.findByText(/Tableau de bord/i)).toBeInTheDocument();
  expect(container).toMatchSnapshot();
});

test("Dashboard component : Should displays correctly with electricity values", async () => {
  axios.get = jest.fn()
    .mockReturnValueOnce( Promise.resolve({ data: axiosGetMetersElec }) )
    .mockReturnValueOnce( Promise.resolve({ data: axiosGetPodElec }) );
 
    const { container } = render(<Dashboard />);
    expect(await screen.findByText(/Tableau de bord/i)).toBeInTheDocument();
    expect(container).toMatchSnapshot();
});

test("Dashboard component : Should displays correctly with all values", async () => {
  axios.get = jest.fn()
    .mockReturnValueOnce( Promise.resolve({ data: axiosGetMetersBoth }) )
    .mockReturnValueOnce( Promise.resolve({ data: axiosGetPodGas }) )
    .mockReturnValueOnce( Promise.resolve({ data: axiosGetPodElec }) );
 
    const { container } = render(<Dashboard />);
    expect(await screen.findByText(/Tableau de bord/i)).toBeInTheDocument();
    expect(container).toMatchSnapshot();
});
