import { format } from 'date-fns';
import classnames from 'classnames';

import './ConsoTable.css';

type ConsoTableProps = {
  header?: Array<string>,
  values?: Array<ConsoValues>,
  className?: string,
}

type ConsoValues = {
  id: string,
  date: string,
  values?: Array<number>,
}

type ConsoRowProps = {
  rowValues: ConsoValues,
}

const ConsoRow = ({ rowValues }: ConsoRowProps) => {
  const { date, values = [], id } = rowValues;
  return (
    <tr className='ConsoRow__row'>
      <td className='ConsoRow__element'>{id}</td>
      <td className='ConsoRow__element'>{format(new Date(date), 'dd/MM/yyyy')}</td>
      {values.map((value, index) => <td className='ConsoRow__element' key={`td-${index}`}>{value}</td>)}
    </tr>
)};

const ConsoTable = ({ header = [], values = [], className }: ConsoTableProps) => {
  if (!values.length && !header.length) return null;
  
  return (
    <div className={classnames('ConsoTable', className)}>
      <table>
        {!!header.length && <thead>
          <tr className='ConsoTable__row'>
            { header.map((label, index) => <th className='ConsoTable__header' key={`th-${index}`}>{label}</th>) }
          </tr>
        </thead>}
        {!!values.length && <tbody>
          {values.map((row, index) => <ConsoRow key={`row-${index}`} rowValues={row} /> )}
        </tbody>}
      </table>
    </div>
  )
};

export default ConsoTable;
