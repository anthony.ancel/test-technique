import { render } from "@testing-library/react";
import ConsoTable from "./ConsoTable";

const mockProps = {
  header: ['id', 'date', 'label1', 'label2', 'label3'],
  values: [{id: '1', date: '2021-12-08T08:58:31.521Z', values: [1, 2, 3]}, {id: '2', date: '2021-12-08T08:58:31.521Z', values: [4, 5, 6]}],
  className: 'mock-classname',
};

test("ConsoTable component : Should displays correctly without props", () => {
  const { container } = render(<ConsoTable />);
  expect(container).toMatchSnapshot();
});

test("ConsoTable component : Should displays correctly with props", () => {
  const { container } = render(<ConsoTable {...mockProps} />);
  expect(container).toMatchSnapshot();
});
